import re
import requests
from config import *
from pymongo import MongoClient


class Connector():
    def __init__(self):
        self.client = MongoClient(MONGODB_URI)
        self.db = self.client[DB_NAME]
        self.collection = self.db['outfits']

    def get_outfits(self, author=None, ship=None, url=None):
        filter_dict = {}
        if author:
            filter_dict['author'] = author
        if ship:
            filter_dict['ship'] = ship
        if url:
            filter_dict['url'] = url
        outfits = []
        for outfit in self.collection.find(filter_dict):
            outfits.append(
                Outfit(
                    author=outfit['author'],
                    url=outfit['url'],
                    ship=outfit['ship'],
                    description=outfit['description'],
                )
            )
        return outfits

    def add_outfit(self, data):
        if not self.get_outfits(url=data.get('url')):
            self.collection.insert_one(data)
            return 'Outfit added successfuly'
        else:
            return 'This outfit already exists'

    def get_ships_count(self, ship_type):
        outfits = self.get_outfits(ship=ship_type)
        return len(outfits)

    def get_help_message(self):
        data = 'Ship types:\n'
        for ship_type in SHIP_LIST_SHORT:
            ship_count = self.get_ships_count(ship_type)
            if ship_count:
                data = '{} <b>{}[{}]</b>,'.format(data, ship_type, ship_count)
            else:
                data = '{} {},'.format(data, ship_type)
        return data[:-1]


class Outfit():
    def __init__(self, author, url, ship, description):
        self.author = author
        self.url = url
        self.ship = ship
        self.description = description

    @staticmethod
    def get_ship_type(url):
        pattern = re.compile("https://.*coriolis\.io/outfit/(?P<ship_name>[a-zA-Z0-9_]+)\?.*")
        r = requests.get(url)
        if r.history and r.history[0].status_code == 301:
            full_url = r.history[0].headers['Location']
            ship_name = pattern.findall(full_url)[0]
        return SHIP_LIST_SHORT[SHIP_LIST_CORIOLIS.index(ship_name)]

    def get_outfit_message(self):
        data = '{}\n==========\n{}\n"{}" by @{}\n'.format(
                    SHIP_LIST[SHIP_LIST_SHORT.index(self.ship)],
                    self.url,
                    self.description,
                    self.author,
                )
        return data

    def get_outfit_message_by_author(self):
        data = '{}\n{}\n"{}"\n'.format(
                    self.ship,
                    self.url,
                    self.description,
                )
        return data
