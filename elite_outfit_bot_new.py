import asyncio
import logging
import re

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher, filters
from aiogram.utils.executor import start_webhook
from aiogram.types.reply_keyboard import ReplyKeyboardMarkup, KeyboardButton
from aiogram.types.inline_keyboard import InlineKeyboardMarkup, InlineKeyboardButton
from config import *
from helpers import *


logging.basicConfig(level=logging.INFO)

loop = asyncio.get_event_loop()
bot = Bot(token=TOKEN, loop=loop)
dp = Dispatcher(bot)

# Inline Mode
# ----------------------------------------------
# @dp.inline_handler()
# async def inline_handler(inline_query: types.InlineQuery):
#     input_content = types.InputTextMessageContent(inline_query.query)
#     items = []
#     if input_content.message_text == 'outfit':
#         #outfits = db.get_outfits(ship='fdl')
#         for i, ship in enumerate(SHIP_LIST_SHORT):
#             input_content = types.InputTextMessageContent(ship)
#             items.append(
#                 types.InlineQueryResultArticle(
#                     id=str(i),
#                     title=ship,
#                     input_message_content=input_content)
#             )
#     await bot.answer_inline_query(inline_query.id, results=items, cache_time=1)


async def on_startup(dp):
    await bot.set_webhook(WEBHOOK_URL)
    # insert code here to run it after start


async def on_shutdown(dp):
    # insert code here to run it before shutdown
    pass


if __name__ == '__main__':
    start_webhook(dispatcher=dp, webhook_path=WEBHOOK_PATH, on_startup=on_startup, on_shutdown=on_shutdown,
                  skip_updates=True, host=WEBAPP_HOST, port=WEBAPP_PORT)