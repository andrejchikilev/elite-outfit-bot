import asyncio
import logging
import re

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher, filters
from aiogram.utils.executor import start_webhook
from aiogram.types.reply_keyboard import ReplyKeyboardMarkup, KeyboardButton
from config import *
from models import *


logging.basicConfig(level=logging.INFO)

loop = asyncio.get_event_loop()
bot = Bot(token=TOKEN, loop=loop)
dp = Dispatcher(bot)
db = Connector()


# PM Mode
# ----------------------------------------------
@dp.message_handler(commands=['start', 'welcome', 'about', 'help'])
async def cmd_handler(message: types.Message):
    data = '''Hello! I am Elite Dangerous outfits store bot.

/outfit - ship types list,
/outfit <ship_type> - search by ship type,
/outfit <@username> - search by author,
/outfit add <short_coriolis_link> <description> - add new outfit.'''
    try:
        await bot.delete_message(message.chat.id, message.message_id)
    except:
        return
    await bot.send_message(message.from_user.id, data)


# return button list of ships otfits
@dp.message_handler(filters.RegexpCommandsFilter(regexp_commands=['outfit$']))
async def outfit_handler(message: types.Message):
    buttons = {ship_type: KeyboardButton(text='{} ({})'.format(ship_type, db.get_ships_count(ship_type))) for ship_type in SHIP_LIST_SHORT}
    row_width = 3
    markup = ReplyKeyboardMarkup(row_width=row_width, one_time_keyboard=True)
    i = 0
    for ship_type in SHIP_LIST_SHORT:
        if db.get_ships_count(ship_type):
            if i % row_width:
                markup.insert(buttons[ship_type])
            else:
                markup.row()
                markup.insert(buttons[ship_type])
            i += 1
    try:
        await bot.delete_message(message.chat.id, message.message_id)
    except:
        return
    await bot.send_message(message.from_user.id, db.get_help_message(), reply_markup=markup, parse_mode='HTML')


# add new outfit "/outfit add <link> Description"
@dp.message_handler(filters.RegexpCommandsFilter(regexp_commands=['outfit add https://s.orbis.zone/[a-zA-Z0-9_-]+ .+']))
async def outfit_add_handler(message: types.Message):
    result = re.split(r' ', message.text)
    outfit_url = re.match(r'https://s.orbis.zone/[a-zA-Z0-9_-]+', result[2]).group(0)
    ship_type = Outfit.get_ship_type(outfit_url)
    description = re.sub(
        r'^/outfit add https://s.orbis.zone/[a-zA-Z0-9_-]+ ',
        '',
        message.text,
    )
    if ship_type and ship_type in SHIP_LIST_SHORT:
        data = {
            'author': message.from_user.username,
            'url': outfit_url,
            'ship': ship_type,
            'description': description,
        }
        result_message = db.add_outfit(data)
        await bot.send_message(message.from_user.id, result_message)
    else:
        await bot.send_message(message.from_user.id, db.get_help_message())


# return all ship-typed outfits
@dp.message_handler(filters.RegexpCommandsFilter(regexp_commands=['outfit [a-zA-Z0-9_]+$']))
async def outfit_ship_handler(message: types.Message):
    result = re.split(r' ', message.text)
    ship_name = result[1]
    if ship_name in SHIP_LIST_SHORT:
        outfits = db.get_outfits(ship=ship_name)
        for outfit in outfits:
            data = outfit.get_outfit_message()
            await bot.send_message(
                message.from_user.id, 
                data,
                disable_web_page_preview=True,
            )
    try:
        await bot.delete_message(message.chat.id, message.message_id)
    except:
        return


@dp.message_handler(regexp='^[a-zA-Z0-9_]+')
async def ship_name_handler(message: types.Message):
    result = re.split(r' ', message.text)
    ship_name = result[0]
    if ship_name in SHIP_LIST_SHORT:
        outfits = db.get_outfits(ship=ship_name)
        for outfit in outfits:
            data = outfit.get_outfit_message()
            await bot.send_message(
                message.from_user.id, 
                data,
                disable_web_page_preview=True,
            )
    try:
        await bot.delete_message(message.chat.id, message.message_id)
    except:
        return


# return per-user otfits
@dp.message_handler(filters.RegexpCommandsFilter(regexp_commands=['outfit @[a-zA-Z_0-9]+$']))
async def outfit_author_handler(message: types.Message):
    result = re.split(r' ', message.text)
    username = result[1]
    outfits = sorted(db.get_outfits(author=username[1:]), key=lambda outfit: outfit.ship)
    for outfit in outfits:
        data = outfit.get_outfit_message()
        await bot.send_message(
            message.from_user.id,
            data,
            disable_web_page_preview=True,
        )
    try:
        await bot.delete_message(message.chat.id, message.message_id)
    except:
        return

async def on_startup(dp):
    await bot.set_webhook(WEBHOOK_URL)
    # insert code here to run it after start


async def on_shutdown(dp):
    # insert code here to run it before shutdown
    pass


if __name__ == '__main__':
    start_webhook(dispatcher=dp, webhook_path=WEBHOOK_PATH, on_startup=on_startup, on_shutdown=on_shutdown,
                  skip_updates=True, host=WEBAPP_HOST, port=WEBAPP_PORT)